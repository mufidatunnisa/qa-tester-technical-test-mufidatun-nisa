*** Settings ***
Documentation     Simple example using SeleniumLibrary.
Library           SeleniumLibrary
Resource  ../variableGlobal.robot

*** Test Cases ***
fill all column correctly
    Open Browser  ${url}  ${browser}
    maximize browser window
    sleep  5
    input text  //input[@name='signatory_first_name']  12324398493
    input text  //input[@name='signatory_last_name']  Nisa
    click element  //div[@id='e-mail']
    input text  //input[@name='email']  fidahani25@gmail.com
    click element  //div[@id='pass']
    input text  //input[@name='password']  Fida2119.
    input text  //input[@name='name']  PT. GOOD NEWS FROM INDONESIA
    input text  //input[@type='tel']  82336038302
    click element  xpath=//select[@name="country"]/option[@value=9]
    input text  //input[@name='state']  Jawa Timur
    Click Element  //input[@type='checkbox']
    Click Element  //button[@id='btn-register']
    sleep  5
    String message = driver.findElement(By.name("signatory_first_name")).getAttribute("validationMessage");  
    sleep  3
    close browser